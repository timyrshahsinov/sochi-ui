import { StoryFn, Meta } from '@storybook/vue3';
// import Button from './Button.vue';
import topGirl from './topGirl.vue'

export default {
  title: 'topGirl',
  component: topGirl,
  argTypes: {
    girl: {
      control: { type: 'Object' },
    },
    backUrl: {
      control: {
        type: 'String'
      },
      defaultValue: 'https://api.sochi-night.net/',
    }
  },
} as Meta<typeof topGirl>;

const Template: StoryFn<typeof topGirl> = (args) => ({
  components: { topGirl },
  setup() {
    return { args };
  },
  template: `
    <div style="display: flex; flex-direction: row; gap: 12px;">
    <topGirl />
    </div>
  `,
});

export const DefaultButton: StoryFn<typeof topGirl> = (args) => ({
  components: { topGirl },
  setup() {
    return { args };
  },
  template: '',
});
