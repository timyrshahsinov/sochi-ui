import { StoryFn, Meta } from '@storybook/vue3';
// import Button from './Button.vue';
import mainCover from './mainCover.vue'

export default {
  title: 'mainCover',
  component: mainCover,
  argTypes: {
    title: {
      control: { type: 'String' },
      defaultValue: 'Топ 30 sochi-night',
    },
  },
} as Meta<typeof mainCover>;

const Template: StoryFn<typeof mainCover> = (args) => ({
  components: { mainCover },
  setup() {
    return { args };
  },
  template: `
    <div style="display: flex; flex-direction: row; gap: 12px;">
    <mainCover />
    </div>
  `,
});

export const DefaultButton: StoryFn<typeof mainCover> = (args) => ({
  components: { mainCover },
  setup() {
    return { args };
  },
  template: '<div></div>',
});
