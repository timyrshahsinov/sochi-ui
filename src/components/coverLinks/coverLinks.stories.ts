import { StoryFn, Meta } from '@storybook/vue3';
// import Button from './Button.vue';
import coverLinks from './coverLinks.vue'

export default {
  title: 'coverLinks',
  component: coverLinks,
  argTypes: {
    title: {
      control: { type: 'String' },
      defaultValue: 'Топ 30 sochi-night',
    },
  },
} as Meta<typeof coverLinks>;

const Template: StoryFn<typeof coverLinks> = (args) => ({
  components: { coverLinks },
  setup() {
    return { args };
  },
  template: `
    <div style="display: flex; flex-direction: row; gap: 12px;">
    <coverLinks />
    </div>
  `,
});

export const DefaultButton: StoryFn<typeof coverLinks> = (args) => ({
  components: { coverLinks },
  setup() {
    return { args };
  },
  template: '<div></div>',
});
