import Button from './button/Button.vue';
import MyComponent from './mycomponent/mycomponent.vue';
import topGirls from './topGirls/topGirls.vue';
import topGirl from './topGirl/topGirl.vue';
import mainCover from './mainCover/mainCover.vue'
import coverLinks from './coverLinks/coverLinks.vue'
import coverLink from './coverLink/coverLink.vue'
import coverBtn from './coverBtn/coverBtn.vue'

export {
  Button, MyComponent, topGirls, topGirl, mainCover, coverLinks, coverLink, coverBtn
};
