import { StoryFn, Meta } from '@storybook/vue3';
// import Button from './Button.vue';
import topGirls from './topGirls.vue'

export default {
  title: 'topGirls',
  component: topGirls,
  argTypes: {
    title: {
      control: { type: 'String' },
      defaultValue: 'Топ 30 sochi-night',
    },
  },
} as Meta<typeof topGirls>;

const Template: StoryFn<typeof topGirls> = (args) => ({
  components: { topGirls },
  setup() {
    return { args };
  },
  template: `
    <div style="display: flex; flex-direction: row; gap: 12px;">
    <topGirls />
    </div>
  `,
});

export const DefaultButton: StoryFn<typeof topGirls> = (args) => ({
  components: { topGirls },
  setup() {
    return { args };
  },
  template: '<Button v-bind="args">Button</Button>',
});
