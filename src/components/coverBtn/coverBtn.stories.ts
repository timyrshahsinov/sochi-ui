import { StoryFn, Meta } from '@storybook/vue3';
// import Button from './Button.vue';
import coverBtn from './coverBtn.vue'

export default {
  title: 'coverBtn',
  component: coverBtn,
  argTypes: {
    title: {
      control: { type: 'String' },
      defaultValue: 'Топ 30 sochi-night',
    },
  },
} as Meta<typeof coverBtn>;

const Template: StoryFn<typeof coverBtn> = (args) => ({
  components: { coverBtn },
  setup() {
    return { args };
  },
  template: `
    <div style="display: flex; flex-direction: row; gap: 12px;">
    <coverBtn />
    </div>
  `,
});

export const DefaultButton: StoryFn<typeof coverBtn> = (args) => ({
  components: { coverBtn },
  setup() {
    return { args };
  },
  template: '<div></div>',
});
