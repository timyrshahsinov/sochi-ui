import { StoryFn, Meta } from '@storybook/vue3';
// import Button from './Button.vue';
import coverLink from './coverLink.vue'

export default {
  title: 'coverLink',
  component: coverLink,
  argTypes: {
    title: {
      control: { type: 'String' },
      defaultValue: 'Топ 30 sochi-night',
    },
  },
} as Meta<typeof coverLink>;

const Template: StoryFn<typeof coverLink> = (args) => ({
  components: { coverLink },
  setup() {
    return { args };
  },
  template: `
    <div style="display: flex; flex-direction: row; gap: 12px;">
    <coverLink />
    </div>
  `,
});

export const DefaultButton: StoryFn<typeof coverLink> = (args) => ({
  components: { coverLink },
  setup() {
    return { args };
  },
  template: '<div></div>',
});
