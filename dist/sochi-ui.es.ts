import { defineComponent as l, openBlock as i, createElementBlock as o, normalizeClass as v, renderSlot as a, createElementVNode as e, toDisplayString as _, createTextVNode as m, createCommentVNode as d, createStaticVNode as g } from "vue";
const y = { class: "button__text" }, tt = /* @__PURE__ */ l({
  __name: "Button",
  props: {
    text: {
      type: String
    },
    type: {
      type: String,
      default: "primary"
    }
  },
  setup(t) {
    return (n, s) => (i(), o("button", {
      class: v(["button", [t.type]])
    }, [
      a(n.$slots, "left-icon"),
      e("p", y, _(t.text), 1),
      a(n.$slots, "rigth-icon")
    ], 2));
  }
});
const et = /* @__PURE__ */ l({
  __name: "mycomponent",
  props: {
    title: {
      type: String,
      default: !1
    },
    content: {
      type: String,
      default: !1
    }
  },
  setup(t) {
    const n = t;
    return (s, c) => (i(), o("div", null, [
      e("h1", null, _(n.title), 1),
      e("p", null, _(n.content), 1)
    ]));
  }
}), f = { class: "top-girls" }, $ = { class: "top-girls__container" }, C = { class: "top-girls__title" }, k = { class: "top-girls__content" }, x = { class: "top-girls__items" }, nt = /* @__PURE__ */ l({
  __name: "topGirls",
  props: {
    title: {
      type: String,
      default: "Топ 30 sochi-night"
    }
  },
  setup(t) {
    return (n, s) => (i(), o("div", f, [
      e("div", $, [
        e("p", C, _(t.title), 1),
        e("div", k, [
          e("div", x, [
            a(n.$slots, "default")
          ])
        ])
      ])
    ]));
  }
});
const b = { class: "top-girl__container" }, w = { class: "top-girl__img" }, S = ["src"], B = /* @__PURE__ */ e("div", { class: "top-girl__overlay" }, null, -1), L = { class: "top-girl__content" }, T = { class: "top-girl__bottom" }, G = { class: "top-girl__bottom-title" }, I = {
  key: 0,
  class: "top-girl__call btn"
}, q = {
  key: 1,
  class: "top-girl__status"
}, N = /* @__PURE__ */ g('<svg width="6" height="6" viewBox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg"><g clip-path="url(#clip0_361_59)"><path fill-rule="evenodd" clip-rule="evenodd" d="M11.6706 2.76843C11.8229 2.9208 11.9085 3.12742 11.9085 3.34287C11.9085 3.55832 11.8229 3.76494 11.6706 3.91731L5.58064 10.0073C5.50016 10.0878 5.40461 10.1516 5.29945 10.1952C5.19429 10.2388 5.08158 10.2612 4.96775 10.2612C4.85392 10.2612 4.7412 10.2388 4.63604 10.1952C4.53088 10.1516 4.43533 10.0878 4.35485 10.0073L1.3291 6.98206C1.2515 6.90711 1.1896 6.81745 1.14702 6.71832C1.10444 6.6192 1.08202 6.51258 1.08109 6.4047C1.08015 6.29681 1.10071 6.18982 1.14156 6.08997C1.18241 5.99012 1.24274 5.8994 1.31903 5.82311C1.39532 5.74682 1.48604 5.68649 1.58589 5.64564C1.68574 5.60479 1.79273 5.58423 1.90061 5.58517C2.0085 5.5861 2.11511 5.60852 2.21424 5.6511C2.31337 5.69368 2.40303 5.75558 2.47798 5.83318L4.96748 8.32268L10.5212 2.76843C10.5966 2.69293 10.6862 2.63304 10.7848 2.59217C10.8835 2.55131 10.9892 2.53027 11.0959 2.53027C11.2026 2.53027 11.3083 2.55131 11.4069 2.59217C11.5056 2.63304 11.5951 2.69293 11.6706 2.76843Z" fill="white"></path></g><defs><clipPath id="clip0_361_59"><rect width="13" height="13" fill="white"></rect></clipPath></defs></svg><p>Проверено</p>', 2), V = [
  N
], st = /* @__PURE__ */ l({
  __name: "topGirl",
  props: {
    girl: {
      type: Object,
      required: !0
      // исправлено с require на required
    },
    backUrl: {
      type: String,
      default: "https://api.sochi-night.net",
      required: !0
    }
  },
  setup(t) {
    const n = t;
    function s() {
      const r = n.girl.hour_apartment_price ?? 1 / 0, p = n.girl.two_hours_apartment_price ?? 1 / 0, u = n.girl.hour_departure_price ?? 1 / 0, h = n.girl.two_hours_departure_price ?? 1 / 0;
      return Math.min(
        r,
        p,
        u,
        h
      );
    }
    function c(r) {
      const p = /* @__PURE__ */ new Date(), u = new Date(r);
      return p.getTime() > u.getTime();
    }
    return (r, p) => (i(), o("div", b, [
      e("div", w, [
        e("img", {
          src: t.backUrl + t.girl.images[0].url
        }, null, 8, S)
      ]),
      B,
      e("div", L, [
        e("div", T, [
          e("div", G, _(t.girl.name), 1),
          e("p", null, [
            m(" от "),
            e("span", null, _(s()), 1)
          ])
        ]),
        c(t.girl.free_until) ? d("", !0) : (i(), o("button", I, " Жду звонка ")),
        t.girl.state == "accepted" ? (i(), o("button", q, V)) : d("", !0)
      ])
    ]));
  }
});
const D = { class: "main-cover" }, M = { class: "main-cover__container" }, P = { class: "main-cover__titles" }, E = { class: "main-cover__subtitle" }, O = { class: "main-cover__title" }, U = { class: "main-cover__content" }, j = { class: "main-cover__links" }, z = { class: "main-cover__btns" }, A = { class: "main-cover__img" }, Z = ["src"], it = /* @__PURE__ */ l({
  __name: "mainCover",
  props: {
    title: {
      type: String,
      require: !0
    },
    subtitle: {
      type: String,
      require: !0
    },
    locations: {
      type: Array
    },
    pathToImg: {
      type: String
    }
  },
  setup(t) {
    return (n, s) => (i(), o("div", D, [
      e("div", M, [
        e("div", P, [
          e("p", E, _(t.subtitle), 1),
          e("p", O, _(t.title), 1)
        ]),
        e("div", U, [
          e("div", j, [
            a(n.$slots, "links")
          ]),
          e("div", z, [
            a(n.$slots, "btns")
          ])
        ])
      ]),
      e("div", A, [
        e("img", {
          src: t.pathToImg,
          alt: "main-cover"
        }, null, 8, Z)
      ])
    ]));
  }
});
const F = (t, n) => {
  const s = t.__vccOpts || t;
  for (const [c, r] of n)
    s[c] = r;
  return s;
}, H = {}, J = { class: "cover-links" }, K = { class: "cover-links__container" };
function Q(t, n) {
  return i(), o("div", J, [
    e("div", K, [
      a(t.$slots, "default")
    ])
  ]);
}
const ot = /* @__PURE__ */ F(H, [["render", Q]]), R = { class: "cover-link" }, W = { class: "cover-link__content" }, _t = /* @__PURE__ */ l({
  __name: "coverLink",
  props: {
    link: {
      type: String
    }
  },
  setup(t) {
    return (n, s) => (i(), o("div", R, [
      e("p", W, _(t.link), 1)
    ]));
  }
});
const ct = /* @__PURE__ */ l({
  __name: "coverBtn",
  props: {
    text: {
      type: String
    }
  },
  emits: ["on-click"],
  setup(t, { emit: n }) {
    function s(c) {
      n("on-click", c);
    }
    return (c, r) => (i(), o("button", {
      class: "cover-btn",
      onClick: s
    }, _(t.text), 1));
  }
});
export {
  tt as Button,
  et as MyComponent,
  ct as coverBtn,
  _t as coverLink,
  ot as coverLinks,
  it as mainCover,
  st as topGirl,
  nt as topGirls
};
