declare const _default: import("vue").DefineComponent<{
    link: {
        type: StringConstructor;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    link: {
        type: StringConstructor;
    };
}>>, {}, {}>;
export default _default;
