declare const _default: import("vue").DefineComponent<{
    girl: {
        type: ObjectConstructor;
        required: true;
    };
    backUrl: {
        type: StringConstructor;
        default: string;
        required: true;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    girl: {
        type: ObjectConstructor;
        required: true;
    };
    backUrl: {
        type: StringConstructor;
        default: string;
        required: true;
    };
}>>, {
    backUrl: string;
}, {}>;
export default _default;
