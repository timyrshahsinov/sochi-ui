import { PropType } from 'vue';
declare const _default: __VLS_WithTemplateSlots<import("vue").DefineComponent<{
    text: {
        type: StringConstructor;
    };
    type: {
        type: PropType<"primary" | "secondary" | "orange">;
        default: string;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    text: {
        type: StringConstructor;
    };
    type: {
        type: PropType<"primary" | "secondary" | "orange">;
        default: string;
    };
}>>, {
    type: "primary" | "secondary" | "orange";
}, {}>, {
    "left-icon"?(_: {}): any;
    "rigth-icon"?(_: {}): any;
}>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & {
    new (): {
        $slots: S;
    };
};
