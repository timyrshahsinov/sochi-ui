declare const _default: import("vue").DefineComponent<{
    text: {
        type: StringConstructor;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "on-click"[], "on-click", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    text: {
        type: StringConstructor;
    };
}>> & {
    "onOn-click"?: ((...args: any[]) => any) | undefined;
}, {}, {}>;
export default _default;
