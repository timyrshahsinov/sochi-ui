declare const _default: __VLS_WithTemplateSlots<import("vue").DefineComponent<{
    title: {
        type: StringConstructor;
        require: boolean;
    };
    subtitle: {
        type: StringConstructor;
        require: boolean;
    };
    locations: {
        type: ArrayConstructor;
    };
    pathToImg: {
        type: StringConstructor;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    title: {
        type: StringConstructor;
        require: boolean;
    };
    subtitle: {
        type: StringConstructor;
        require: boolean;
    };
    locations: {
        type: ArrayConstructor;
    };
    pathToImg: {
        type: StringConstructor;
    };
}>>, {}, {}>, {
    links?(_: {}): any;
    btns?(_: {}): any;
}>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & {
    new (): {
        $slots: S;
    };
};
