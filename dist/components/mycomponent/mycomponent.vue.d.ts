declare const _default: import("vue").DefineComponent<{
    title: {
        type: StringConstructor;
        default: boolean;
    };
    content: {
        type: StringConstructor;
        default: boolean;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    title: {
        type: StringConstructor;
        default: boolean;
    };
    content: {
        type: StringConstructor;
        default: boolean;
    };
}>>, {
    title: string;
    content: string;
}, {}>;
export default _default;
